import pytest

from marmiton import Marmiton, RecipeNotFound
from src.domain.recipes import get_recipes
from src.domain.streamlit_classes import Recipes


def test_get_recipes_class_raise_error_on_empty_list(monkeypatch):
    
    def mock_query_marmiton(x):
        return []
    
    monkeypatch.setattr(Marmiton, "search", mock_query_marmiton)

    err_mess = "function does not react correctly when Marmiton.search returns empty list and raises IndexError."

    rc = Recipes()
    try:
        rc.get_recipes()
    except IndexError :
        pytest.fail(err_mess)
