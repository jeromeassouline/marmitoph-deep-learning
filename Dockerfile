FROM python:3.7-slim
COPY requirements.txt ./requirements.txt
RUN pip install -r requirements.txt
ENV APP_HOME /src
WORKDIR $APP_HOME
#ENV PYTHONPATH .
COPY . ./
CMD streamlit src/application/marmitoph_app.py --server.port $PORT
